var express = require('express');
var router = express.Router();
const axios = require("axios")

router.get('/nse', async (req, res) => {
    const NseData = await axios.get('https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY')
    return res.status(200).json(NseData.data)
});

module.exports = router;


