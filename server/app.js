var express = require('express');
var path = require('path');
const cors = require('cors')

var nseRouter = require('./nseRouter');

var app = express();

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', nseRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.status(404).json({ success: false, data: [], message: "App does not have this route" })
    // next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    console.log(err);
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.listen(4000)
module.exports = app;
