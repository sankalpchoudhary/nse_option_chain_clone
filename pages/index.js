import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { Dropdown } from 'semantic-ui-react'
import { useEffect, useState } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';

const Options = [
  { key: 'nifty', value: 'NIFTY', text: 'NIFTY' },
  { key: 'banknifty', value: 'BANKNIFTY', text: 'BANKNIFTY' },
  { key: 'finnifty', value: 'FINNIFTY', text: 'FINNIFTY' },
  { key: 'midcpnifty', value: 'MIDCPNIFTY', text: 'MIDCPNIFTY' },
]

const Symbol = [
  {
    "key": "aartiind",
    "value": "AARTIIND",
    "text": "AARTIIND"
  },
  {
    "key": "abb",
    "value": "ABB",
    "text": "ABB"
  },
  {
    "key": "abbotindia",
    "value": "ABBOTINDIA",
    "text": "ABBOTINDIA"
  },
  {
    "key": "abcapital",
    "value": "ABCAPITAL",
    "text": "ABCAPITAL"
  },
  {
    "key": "abfrl",
    "value": "ABFRL",
    "text": "ABFRL"
  },
  {
    "key": "acc",
    "value": "ACC",
    "text": "ACC"
  },
  {
    "key": "adanient",
    "value": "ADANIENT",
    "text": "ADANIENT"
  },
  {
    "key": "adaniports",
    "value": "ADANIPORTS",
    "text": "ADANIPORTS"
  },
  {
    "key": "alkem",
    "value": "ALKEM",
    "text": "ALKEM"
  },
  {
    "key": "amarajabat",
    "value": "AMARAJABAT",
    "text": "AMARAJABAT"
  },
  {
    "key": "ambujacem",
    "value": "AMBUJACEM",
    "text": "AMBUJACEM"
  },
  {
    "key": "apollohosp",
    "value": "APOLLOHOSP",
    "text": "APOLLOHOSP"
  },
  {
    "key": "apollotyre",
    "value": "APOLLOTYRE",
    "text": "APOLLOTYRE"
  },
  {
    "key": "ashokley",
    "value": "ASHOKLEY",
    "text": "ASHOKLEY"
  },
  {
    "key": "asianpaint",
    "value": "ASIANPAINT",
    "text": "ASIANPAINT"
  },
  {
    "key": "astral",
    "value": "ASTRAL",
    "text": "ASTRAL"
  },
  {
    "key": "atul",
    "value": "ATUL",
    "text": "ATUL"
  },
  {
    "key": "aubank",
    "value": "AUBANK",
    "text": "AUBANK"
  },
  {
    "key": "auropharma",
    "value": "AUROPHARMA",
    "text": "AUROPHARMA"
  },
  {
    "key": "axisbank",
    "value": "AXISBANK",
    "text": "AXISBANK"
  },
  {
    "key": "bajaj-auto",
    "value": "BAJAJ-AUTO",
    "text": "BAJAJ-AUTO"
  },
  {
    "key": "bajajfinsv",
    "value": "BAJAJFINSV",
    "text": "BAJAJFINSV"
  },
  {
    "key": "bajfinance",
    "value": "BAJFINANCE",
    "text": "BAJFINANCE"
  },
  {
    "key": "balkrisind",
    "value": "BALKRISIND",
    "text": "BALKRISIND"
  },
  {
    "key": "balramchin",
    "value": "BALRAMCHIN",
    "text": "BALRAMCHIN"
  },
  {
    "key": "bandhanbnk",
    "value": "BANDHANBNK",
    "text": "BANDHANBNK"
  },
  {
    "key": "bankbaroda",
    "value": "BANKBARODA",
    "text": "BANKBARODA"
  },
  {
    "key": "bataindia",
    "value": "BATAINDIA",
    "text": "BATAINDIA"
  },
  {
    "key": "bel",
    "value": "BEL",
    "text": "BEL"
  },
  {
    "key": "bergepaint",
    "value": "BERGEPAINT",
    "text": "BERGEPAINT"
  },
  {
    "key": "bharatforg",
    "value": "BHARATFORG",
    "text": "BHARATFORG"
  },
  {
    "key": "bhartiartl",
    "value": "BHARTIARTL",
    "text": "BHARTIARTL"
  },
  {
    "key": "bhel",
    "value": "BHEL",
    "text": "BHEL"
  },
  {
    "key": "biocon",
    "value": "BIOCON",
    "text": "BIOCON"
  },
  {
    "key": "boschltd",
    "value": "BOSCHLTD",
    "text": "BOSCHLTD"
  },
  {
    "key": "bpcl",
    "value": "BPCL",
    "text": "BPCL"
  },
  {
    "key": "britannia",
    "value": "BRITANNIA",
    "text": "BRITANNIA"
  },
  {
    "key": "bsoft",
    "value": "BSOFT",
    "text": "BSOFT"
  },
  {
    "key": "canbk",
    "value": "CANBK",
    "text": "CANBK"
  },
  {
    "key": "canfinhome",
    "value": "CANFINHOME",
    "text": "CANFINHOME"
  },
  {
    "key": "chamblfert",
    "value": "CHAMBLFERT",
    "text": "CHAMBLFERT"
  },
  {
    "key": "cholafin",
    "value": "CHOLAFIN",
    "text": "CHOLAFIN"
  },
  {
    "key": "cipla",
    "value": "CIPLA",
    "text": "CIPLA"
  },
  {
    "key": "coalindia",
    "value": "COALINDIA",
    "text": "COALINDIA"
  },
  {
    "key": "coforge",
    "value": "COFORGE",
    "text": "COFORGE"
  },
  {
    "key": "colpal",
    "value": "COLPAL",
    "text": "COLPAL"
  },
  {
    "key": "concor",
    "value": "CONCOR",
    "text": "CONCOR"
  },
  {
    "key": "coromandel",
    "value": "COROMANDEL",
    "text": "COROMANDEL"
  },
  {
    "key": "crompton",
    "value": "CROMPTON",
    "text": "CROMPTON"
  },
  {
    "key": "cub",
    "value": "CUB",
    "text": "CUB"
  },
  {
    "key": "cumminsind",
    "value": "CUMMINSIND",
    "text": "CUMMINSIND"
  },
  {
    "key": "dabur",
    "value": "DABUR",
    "text": "DABUR"
  },
  {
    "key": "dalbharat",
    "value": "DALBHARAT",
    "text": "DALBHARAT"
  },
  {
    "key": "deepakntr",
    "value": "DEEPAKNTR",
    "text": "DEEPAKNTR"
  },
  {
    "key": "deltacorp",
    "value": "DELTACORP",
    "text": "DELTACORP"
  },
  {
    "key": "divislab",
    "value": "DIVISLAB",
    "text": "DIVISLAB"
  },
  {
    "key": "dixon",
    "value": "DIXON",
    "text": "DIXON"
  },
  {
    "key": "dlf",
    "value": "DLF",
    "text": "DLF"
  },
  {
    "key": "drreddy",
    "value": "DRREDDY",
    "text": "DRREDDY"
  },
  {
    "key": "eichermot",
    "value": "EICHERMOT",
    "text": "EICHERMOT"
  },
  {
    "key": "escorts",
    "value": "ESCORTS",
    "text": "ESCORTS"
  },
  {
    "key": "exideind",
    "value": "EXIDEIND",
    "text": "EXIDEIND"
  },
  {
    "key": "federalbnk",
    "value": "FEDERALBNK",
    "text": "FEDERALBNK"
  },
  {
    "key": "fsl",
    "value": "FSL",
    "text": "FSL"
  },
  {
    "key": "gail",
    "value": "GAIL",
    "text": "GAIL"
  },
  {
    "key": "glenmark",
    "value": "GLENMARK",
    "text": "GLENMARK"
  },
  {
    "key": "gmrinfra",
    "value": "GMRINFRA",
    "text": "GMRINFRA"
  },
  {
    "key": "gnfc",
    "value": "GNFC",
    "text": "GNFC"
  },
  {
    "key": "godrejcp",
    "value": "GODREJCP",
    "text": "GODREJCP"
  },
  {
    "key": "godrejprop",
    "value": "GODREJPROP",
    "text": "GODREJPROP"
  },
  {
    "key": "granules",
    "value": "GRANULES",
    "text": "GRANULES"
  },
  {
    "key": "grasim",
    "value": "GRASIM",
    "text": "GRASIM"
  },
  {
    "key": "gspl",
    "value": "GSPL",
    "text": "GSPL"
  },
  {
    "key": "gujgasltd",
    "value": "GUJGASLTD",
    "text": "GUJGASLTD"
  },
  {
    "key": "hal",
    "value": "HAL",
    "text": "HAL"
  },
  {
    "key": "havells",
    "value": "HAVELLS",
    "text": "HAVELLS"
  },
  {
    "key": "hcltech",
    "value": "HCLTECH",
    "text": "HCLTECH"
  },
  {
    "key": "hdfc",
    "value": "HDFC",
    "text": "HDFC"
  },
  {
    "key": "hdfcamc",
    "value": "HDFCAMC",
    "text": "HDFCAMC"
  },
  {
    "key": "hdfcbank",
    "value": "HDFCBANK",
    "text": "HDFCBANK"
  },
  {
    "key": "hdfclife",
    "value": "HDFCLIFE",
    "text": "HDFCLIFE"
  },
  {
    "key": "heromotoco",
    "value": "HEROMOTOCO",
    "text": "HEROMOTOCO"
  },
  {
    "key": "hindalco",
    "value": "HINDALCO",
    "text": "HINDALCO"
  },
  {
    "key": "hindcopper",
    "value": "HINDCOPPER",
    "text": "HINDCOPPER"
  },
  {
    "key": "hindpetro",
    "value": "HINDPETRO",
    "text": "HINDPETRO"
  },
  {
    "key": "hindunilvr",
    "value": "HINDUNILVR",
    "text": "HINDUNILVR"
  },
  {
    "key": "honaut",
    "value": "HONAUT",
    "text": "HONAUT"
  },
  {
    "key": "ibulhsgfin",
    "value": "IBULHSGFIN",
    "text": "IBULHSGFIN"
  },
  {
    "key": "icicibank",
    "value": "ICICIBANK",
    "text": "ICICIBANK"
  },
  {
    "key": "icicigi",
    "value": "ICICIGI",
    "text": "ICICIGI"
  },
  {
    "key": "icicipruli",
    "value": "ICICIPRULI",
    "text": "ICICIPRULI"
  },
  {
    "key": "idea",
    "value": "IDEA",
    "text": "IDEA"
  },
  {
    "key": "idfc",
    "value": "IDFC",
    "text": "IDFC"
  },
  {
    "key": "idfcfirstb",
    "value": "IDFCFIRSTB",
    "text": "IDFCFIRSTB"
  },
  {
    "key": "iex",
    "value": "IEX",
    "text": "IEX"
  },
  {
    "key": "igl",
    "value": "IGL",
    "text": "IGL"
  },
  {
    "key": "indhotel",
    "value": "INDHOTEL",
    "text": "INDHOTEL"
  },
  {
    "key": "indiacem",
    "value": "INDIACEM",
    "text": "INDIACEM"
  },
  {
    "key": "indiamart",
    "value": "INDIAMART",
    "text": "INDIAMART"
  },
  {
    "key": "indigo",
    "value": "INDIGO",
    "text": "INDIGO"
  },
  {
    "key": "indusindbk",
    "value": "INDUSINDBK",
    "text": "INDUSINDBK"
  },
  {
    "key": "industower",
    "value": "INDUSTOWER",
    "text": "INDUSTOWER"
  },
  {
    "key": "infy",
    "value": "INFY",
    "text": "INFY"
  },
  {
    "key": "intellect",
    "value": "INTELLECT",
    "text": "INTELLECT"
  },
  {
    "key": "ioc",
    "value": "IOC",
    "text": "IOC"
  },
  {
    "key": "ipcalab",
    "value": "IPCALAB",
    "text": "IPCALAB"
  },
  {
    "key": "irctc",
    "value": "IRCTC",
    "text": "IRCTC"
  },
  {
    "key": "itc",
    "value": "ITC",
    "text": "ITC"
  },
  {
    "key": "jindalstel",
    "value": "JINDALSTEL",
    "text": "JINDALSTEL"
  },
  {
    "key": "jkcement",
    "value": "JKCEMENT",
    "text": "JKCEMENT"
  },
  {
    "key": "jswsteel",
    "value": "JSWSTEEL",
    "text": "JSWSTEEL"
  },
  {
    "key": "jublfood",
    "value": "JUBLFOOD",
    "text": "JUBLFOOD"
  },
  {
    "key": "kotakbank",
    "value": "KOTAKBANK",
    "text": "KOTAKBANK"
  },
  {
    "key": "l&tfh",
    "value": "L&TFH",
    "text": "L&TFH"
  },
  {
    "key": "lalpathlab",
    "value": "LALPATHLAB",
    "text": "LALPATHLAB"
  },
  {
    "key": "lauruslabs",
    "value": "LAURUSLABS",
    "text": "LAURUSLABS"
  },
  {
    "key": "lichsgfin",
    "value": "LICHSGFIN",
    "text": "LICHSGFIN"
  },
  {
    "key": "lt",
    "value": "LT",
    "text": "LT"
  },
  {
    "key": "lti",
    "value": "LTI",
    "text": "LTI"
  },
  {
    "key": "ltts",
    "value": "LTTS",
    "text": "LTTS"
  },
  {
    "key": "lupin",
    "value": "LUPIN",
    "text": "LUPIN"
  },
  {
    "key": "m&m",
    "value": "M&M",
    "text": "M&M"
  },
  {
    "key": "m&mfin",
    "value": "M&MFIN",
    "text": "M&MFIN"
  },
  {
    "key": "manappuram",
    "value": "MANAPPURAM",
    "text": "MANAPPURAM"
  },
  {
    "key": "marico",
    "value": "MARICO",
    "text": "MARICO"
  },
  {
    "key": "maruti",
    "value": "MARUTI",
    "text": "MARUTI"
  },
  {
    "key": "mcdowell-n",
    "value": "MCDOWELL-N",
    "text": "MCDOWELL-N"
  },
  {
    "key": "mcx",
    "value": "MCX",
    "text": "MCX"
  },
  {
    "key": "metropolis",
    "value": "METROPOLIS",
    "text": "METROPOLIS"
  },
  {
    "key": "mfsl",
    "value": "MFSL",
    "text": "MFSL"
  },
  {
    "key": "mgl",
    "value": "MGL",
    "text": "MGL"
  },
  {
    "key": "mindtree",
    "value": "MINDTREE",
    "text": "MINDTREE"
  },
  {
    "key": "motherson",
    "value": "MOTHERSON",
    "text": "MOTHERSON"
  },
  {
    "key": "mphasis",
    "value": "MPHASIS",
    "text": "MPHASIS"
  },
  {
    "key": "mrf",
    "value": "MRF",
    "text": "MRF"
  },
  {
    "key": "muthootfin",
    "value": "MUTHOOTFIN",
    "text": "MUTHOOTFIN"
  },
  {
    "key": "nationalum",
    "value": "NATIONALUM",
    "text": "NATIONALUM"
  },
  {
    "key": "naukri",
    "value": "NAUKRI",
    "text": "NAUKRI"
  },
  {
    "key": "navinfluor",
    "value": "NAVINFLUOR",
    "text": "NAVINFLUOR"
  },
  {
    "key": "nestleind",
    "value": "NESTLEIND",
    "text": "NESTLEIND"
  },
  {
    "key": "nmdc",
    "value": "NMDC",
    "text": "NMDC"
  },
  {
    "key": "ntpc",
    "value": "NTPC",
    "text": "NTPC"
  },
  {
    "key": "oberoirlty",
    "value": "OBEROIRLTY",
    "text": "OBEROIRLTY"
  },
  {
    "key": "ofss",
    "value": "OFSS",
    "text": "OFSS"
  },
  {
    "key": "ongc",
    "value": "ONGC",
    "text": "ONGC"
  },
  {
    "key": "pageind",
    "value": "PAGEIND",
    "text": "PAGEIND"
  },
  {
    "key": "pel",
    "value": "PEL",
    "text": "PEL"
  },
  {
    "key": "persistent",
    "value": "PERSISTENT",
    "text": "PERSISTENT"
  },
  {
    "key": "petronet",
    "value": "PETRONET",
    "text": "PETRONET"
  },
  {
    "key": "pfc",
    "value": "PFC",
    "text": "PFC"
  },
  {
    "key": "pidilitind",
    "value": "PIDILITIND",
    "text": "PIDILITIND"
  },
  {
    "key": "piind",
    "value": "PIIND",
    "text": "PIIND"
  },
  {
    "key": "pnb",
    "value": "PNB",
    "text": "PNB"
  },
  {
    "key": "polycab",
    "value": "POLYCAB",
    "text": "POLYCAB"
  },
  {
    "key": "powergrid",
    "value": "POWERGRID",
    "text": "POWERGRID"
  },
  {
    "key": "pvr",
    "value": "PVR",
    "text": "PVR"
  },
  {
    "key": "rain",
    "value": "RAIN",
    "text": "RAIN"
  },
  {
    "key": "ramcocem",
    "value": "RAMCOCEM",
    "text": "RAMCOCEM"
  },
  {
    "key": "rblbank",
    "value": "RBLBANK",
    "text": "RBLBANK"
  },
  {
    "key": "recltd",
    "value": "RECLTD",
    "text": "RECLTD"
  },
  {
    "key": "reliance",
    "value": "RELIANCE",
    "text": "RELIANCE"
  },
  {
    "key": "sail",
    "value": "SAIL",
    "text": "SAIL"
  },
  {
    "key": "sbicard",
    "value": "SBICARD",
    "text": "SBICARD"
  },
  {
    "key": "sbilife",
    "value": "SBILIFE",
    "text": "SBILIFE"
  },
  {
    "key": "sbin",
    "value": "SBIN",
    "text": "SBIN"
  },
  {
    "key": "shreecem",
    "value": "SHREECEM",
    "text": "SHREECEM"
  },
  {
    "key": "siemens",
    "value": "SIEMENS",
    "text": "SIEMENS"
  },
  {
    "key": "srf",
    "value": "SRF",
    "text": "SRF"
  },
  {
    "key": "srtransfin",
    "value": "SRTRANSFIN",
    "text": "SRTRANSFIN"
  },
  {
    "key": "sunpharma",
    "value": "SUNPHARMA",
    "text": "SUNPHARMA"
  },
  {
    "key": "suntv",
    "value": "SUNTV",
    "text": "SUNTV"
  },
  {
    "key": "syngene",
    "value": "SYNGENE",
    "text": "SYNGENE"
  },
  {
    "key": "tatachem",
    "value": "TATACHEM",
    "text": "TATACHEM"
  },
  {
    "key": "tatacomm",
    "value": "TATACOMM",
    "text": "TATACOMM"
  },
  {
    "key": "tataconsum",
    "value": "TATACONSUM",
    "text": "TATACONSUM"
  },
  {
    "key": "tatamotors",
    "value": "TATAMOTORS",
    "text": "TATAMOTORS"
  },
  {
    "key": "tatapower",
    "value": "TATAPOWER",
    "text": "TATAPOWER"
  },
  {
    "key": "tatasteel",
    "value": "TATASTEEL",
    "text": "TATASTEEL"
  },
  {
    "key": "tcs",
    "value": "TCS",
    "text": "TCS"
  },
  {
    "key": "techm",
    "value": "TECHM",
    "text": "TECHM"
  },
  {
    "key": "titan",
    "value": "TITAN",
    "text": "TITAN"
  },
  {
    "key": "torntpharm",
    "value": "TORNTPHARM",
    "text": "TORNTPHARM"
  },
  {
    "key": "torntpower",
    "value": "TORNTPOWER",
    "text": "TORNTPOWER"
  },
  {
    "key": "trent",
    "value": "TRENT",
    "text": "TRENT"
  },
  {
    "key": "tvsmotor",
    "value": "TVSMOTOR",
    "text": "TVSMOTOR"
  },
  {
    "key": "ubl",
    "value": "UBL",
    "text": "UBL"
  },
  {
    "key": "ultracemco",
    "value": "ULTRACEMCO",
    "text": "ULTRACEMCO"
  },
  {
    "key": "upl",
    "value": "UPL",
    "text": "UPL"
  },
  {
    "key": "vedl",
    "value": "VEDL",
    "text": "VEDL"
  },
  {
    "key": "voltas",
    "value": "VOLTAS",
    "text": "VOLTAS"
  },
  {
    "key": "whirlpool",
    "value": "WHIRLPOOL",
    "text": "WHIRLPOOL"
  },
  {
    "key": "wipro",
    "value": "WIPRO",
    "text": "WIPRO"
  },
  {
    "key": "zeel",
    "value": "ZEEL",
    "text": "ZEEL"
  },
  {
    "key": "zyduslife",
    "value": "ZYDUSLIFE",
    "text": "ZYDUSLIFE"
  }
]

export default function Home() {
  const [rawData, setRawData] = useState([])
  const [ExpiaryDates, setExpiaryDates] = useState([])
  const [strikePrices, setstrikePrices] = useState([])
  const [ExpiaryFilterData, setExpiaryFilterData] = useState([])
  const [TotalData, setTotalData] = useState({
    'Call': { totOI: 0, totVol: 0 },
    'Put': { totOI: 0, totVol: 0 }
  })
  const [ExpiaryDate, setExpiaryDate] = useState([])
  const [TotalCallOI, setTotalCallOI] = useState(0)
  const [TotalPutOI, setTotalPutOI] = useState(0)
  const [TotalCallChangOI, setTotalCallChangOI] = useState(0)
  const [TotalPutChangOI, seTotalPutChangOI] = useState(0)
  const [TotalCallVolumeOI, setTotalCallVolumeOI] = useState(0)
  const [TotalPutVolumeOI, seTotalPutVolumeOI] = useState(0)
  const [BuySellIndicator, setBuySellIndicator] = useState('No Change')
  const [BuySellChgIndicator, setBuySellChgIndicator] = useState('No Change')
  const [SetExpiary, setSetExpiary] = useState(false)

  const columns = [
    {
      name: <div>
        <div>OI</div>
        <div className='text-cyan-900'>{TotalCallOI}</div>
      </div>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell.CE ? cell.CE.openInterest : "_"
        return (
          <>
            {CellData}
          </>
        )
      }
    },
    {
      "name":
        <div>
          <div>Chng in OI</div>
          <div className='text-cyan-900'>{TotalCallChangOI}</div>
        </div>,
      cell: (cell) => {
        const CellData = cell.CE ? cell.CE.changeinOpenInterest : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
      "width": "110px",
    },
    {
      "name": <div>
        <div>Volume</div>
        <div className='text-cyan-900'>{TotalCallVolumeOI}</div>
      </div>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell.CE ? cell.CE.totalTradedVolume : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
    },

    {
      "name": <>LTP</>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell.CE ? cell.CE.lastPrice : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
    },
    {
      "name": <>Strike Price</>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell ? cell.strikePrice : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
    },
    {
      "name": <>LTP</>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell.PE ? cell.PE.lastPrice : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
    },
    {
      "name": <div>
        <div>Volume</div>
        <div className='text-cyan-900'>{TotalPutVolumeOI}</div>
      </div>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell.PE ? cell.PE.totalTradedVolume : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
    },
    {
      "name":
        <div>
          <div>Chng in OI</div>
          <div className='text-cyan-900'>{TotalPutChangOI}</div>
        </div>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell.PE ? cell.PE.changeinOpenInterest : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
    },
    {
      "name": <div>
        <div>OI</div>
        <div className='text-cyan-900'>{TotalPutOI}</div>
      </div>,
      "width": "110px",
      cell: (cell) => {
        const CellData = cell.PE ? cell.PE.openInterest : "_"
        return (
          <>
            {CellData}
          </>
        )
      },
    },
    {
      "name": <div>
        <div>OI PCR Data</div>
        <div className='text-red-900'>{(TotalPutOI / TotalCallOI).toFixed(4)}</div>
      </div>,
      "width": "110px",
      cell: (cell) => {
        const CALLOI = cell.CE ? cell.CE.openInterest : "_"
        const PUTOI = cell.PE ? cell.PE.openInterest : "_"
        const PCR = PUTOI / CALLOI
        return (
          <>
            {PCR.toFixed(2)}
          </>
        )
      },
    },
    {
      "name": <div>
        <div>Change in OI PCR</div>
        <div className='text-red-900'>{(TotalPutChangOI / TotalCallChangOI).toFixed(4)}</div>
      </div>,
      "width": "110px",
      cell: (cell) => {
        const CALLOI = cell.CE ? cell.CE.changeinOpenInterest : "_"
        const PUTOI = cell.PE ? cell.PE.changeinOpenInterest : "_"
        const PCRChangeinOI = PUTOI / CALLOI
        return (
          <>
            {PCRChangeinOI.toFixed(2)}
          </>
        )
      },
    },
    {
      "name": <>OI Minus</>,
      "width": "110px",
      cell: (cell) => {
        const CALLOI = cell.CE ? cell.CE.openInterest : "_"
        const PUTOI = cell.PE ? cell.PE.openInterest : "_"
        const CellData = PUTOI - CALLOI
        return (
          <>
            {CellData.toFixed(2)}
          </>
        )
      },
    },
    {
      "name": <>Change in OI Minus</>,
      "width": "110px",
      cell: (cell) => {
        const CALLOI = cell.CE ? cell.CE.changeinOpenInterest : "_"
        const PUTOI = cell.PE ? cell.PE.changeinOpenInterest : "_"
        const CellData = PUTOI - CALLOI
        return (
          <>
            {CellData.toFixed(2)}
          </>
        )
      },
    },
  ]

  const [Data, setData] = useState([])
  useEffect(() => {
    const FetchData = async () => {
      const OptionChainData = await axios.get('http://127.0.0.1:4000/api/nse')

      const data = []
      for (let index = 0; index < OptionChainData.data.records.expiryDates.length; index++) {
        const element = OptionChainData.data.records.expiryDates[index];
        data.push({
          "key": element,
          "value": element,
          "text": element
        },)
      }
      const STprice = []
      for (let index = 0; index < OptionChainData.data.records.strikePrices.length; index++) {
        const element = OptionChainData.data.records.strikePrices[index];
        STprice.push({
          "key": element,
          "value": element,
          "text": element
        },)

        setstrikePrices(STprice)
      }

      setExpiaryDates(data)
      setData(OptionChainData.data.records.data)
      setRawData(OptionChainData.data)
      setTotalData({ Call: OptionChainData.data.filtered.CE, Put: OptionChainData.data.filtered.PE })
    }
    setInterval(() => {
      FetchData()
    }, 10000);

  }, [])

  useEffect(() => {
    const FilterData = []
    for (let index = 0; index < Data.length; index++) {
      const element = Data[index];
      if (ExpiaryDate == element.expiryDate) {
        FilterData.push(element)
      }
    }

    let totCEOI = 0
    let totPUOI = 0
    let totCEChangOI = 0
    let totPEChangOI = 0
    let totCEVol = 0
    let totPEVol = 0

    for (let index = 0; index < FilterData.length; index++) {
      const element = FilterData[index];
      totCEOI = totCEOI + element.CE?.openInterest
      totCEChangOI = totCEChangOI + element.CE?.changeinOpenInterest
      totCEVol = totCEVol + element.CE?.totalTradedVolume

      totPUOI = totPUOI + element.PE?.openInterest
      totPEChangOI = totPEChangOI + element.PE?.changeinOpenInterest
      totPEVol = totPEVol + element.PE?.totalTradedVolume
    }
    const OLDOIPCR = TotalPutOI / TotalPutOI
    const NEWOIPCR = totPUOI / totCEOI

    const OLDOIChgPCR = TotalPutChangOI / TotalCallChangOI
    const NEWOIChgPCR = totPEChangOI / totCEChangOI

    if (SetExpiary) {
      setBuySellChgIndicator('No change')
    } else {
      if (OLDOIChgPCR == NEWOIChgPCR) {
        setBuySellChgIndicator('No Sigmal')
      }
      if (NEWOIChgPCR > OLDOIChgPCR) {
        setBuySellChgIndicator('Call')
      } else {
        setBuySellChgIndicator('Put')
      }
    }

    if (SetExpiary) {
      setBuySellIndicator('No change')
    } else {
      if (NEWOIPCR == OLDOIPCR) {
        setBuySellIndicator('No Sigmal')
      }
      if (NEWOIPCR > OLDOIPCR) {
        setBuySellIndicator('Call')
      } else {
        setBuySellIndicator('Put')
      }
    }

    setTotalCallOI(totCEOI)
    setTotalCallVolumeOI(totCEVol)
    setTotalCallChangOI(totCEChangOI)
    setTotalPutOI(totPUOI)
    seTotalPutChangOI(totPEChangOI)
    seTotalPutVolumeOI(totPEVol)

    setExpiaryFilterData(FilterData);
    setSetExpiary(false)
  }, [ExpiaryDate, Data, TotalPutOI, TotalPutChangOI, TotalCallChangOI, SetExpiary])


  const changeInExpiary = (e, data) => {
    setExpiaryDate(data.value);
    setSetExpiary(true)
    // setTotalCallOI(0)
    // setTotalCallVolumeOI(0)
    // setTotalCallChangOI(0)
    // setTotalPutOI(0)
    // seTotalPutChangOI(0)
    // seTotalPutVolumeOI(0)
    // setExpiaryFilterData([]);
  }

  return (
    <div>
      <div className='flex my-8 mx-auto'>
        <div className='mx-4'>
          <h2>View Options Contracts for:</h2>
          <Dropdown
            placeholder='Nifty'
            search
            selection
            options={Options}
          />
        </div>
        <div className='mx-4'>
          <h2>Select Symbol</h2>
          <Dropdown
            placeholder='Symbol'
            search
            selection
            options={Symbol}
          />
        </div>

        <div className='mx-4'>
          <h2>Expiry Date</h2>
          <Dropdown
            placeholder='Expiary Date'
            search
            selection
            onChange={changeInExpiary}
            options={ExpiaryDates}
          />
        </div>

        <div className='mx-4'>
          <h2>Strike Price</h2>
          <Dropdown
            placeholder='Strike Price'
            search
            selection

            options={strikePrices}
          />
        </div>
        <h2 className='mr-4'>{BuySellIndicator} / {BuySellChgIndicator}</h2>
      </div>
      <div className='flex'>
      </div>
      <DataTable
        columns={columns}
        data={ExpiaryFilterData}
        fixedHeader
        fixedHeaderScrollHeight="580px"
      />
    </div>
  )
}
